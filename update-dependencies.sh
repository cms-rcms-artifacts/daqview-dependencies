#!/bin/bash
rm -rf node_modules
rm package.json
rm package-lock.json
cp ../daqview-react/package.json ./package.json
cp ../daqview-react/package-lock.json ./package-lock.json
/usr/bin/env npm install
